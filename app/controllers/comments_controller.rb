class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  '''
  def new
    @post = Post.find(params[:post_id])
    @comment = Comment.new
    puts @comment.to_yaml
    puts @post.to_yaml
  end
  '''

  # GET /comments/1/edit
  def edit
    @post = Post.find(params[:post_id])
    @comment
    puts "Editing a comment for:"
    puts "Comment #{@comment.id}"
    puts "Post: #{@post.id}"
    
    # NEED TO DO USER ID CHECKS    
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to post_path(@post), notice: 'Comment was sucessfully updated.' }
      end    
    end
  end

  # POST /comments
  # POST /comments.json
  def create
    # Added, to the get proper post id
    @post = Post.find(params[:post_id])
    @comment = @post.comments.new(comment_params)
    @comment.user_id = current_user.id
    
    respond_to do |format|
      if @comment.save
        format.html { redirect_to post_path(@post), notice: 'Comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comment }
        format.js #do something
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js #do something
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update

    if @comment.user_id != current_user.id
      @message = "You cannot update another user's comment."
      redirect_to @post, notice: @message
      return
    end

    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @post, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy

    @post = Post.find(params[:post_id])    

    if @comment.user_id != current_user.id
      @message = "You cannot delete another user's comment."
      redirect_to @post, notice: @message
      return
    end
 

    @comment.destroy
    respond_to do |format|
      format.html { redirect_to @post }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      #params.require(:comment).permit(:post_id, :body)
      params.require(:comment).permit(:post_id, :body, :user_id)
    end
end
